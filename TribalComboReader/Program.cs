﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.IO;

namespace TribalComboReader
{
    public class Program
    {
        static string inputFile = @"D:\csharpworkspace\TribalAppComboReader\InputFiles\combos.txt";
        static string outputFast = @"D:\csharpworkspace\TribalAppComboReader\InputFiles\fast.txt";
        static string outputSlow = @"D:\csharpworkspace\TribalAppComboReader\InputFiles\slow.txt";

        static void Main(string[] args)
        {
            
            FileHandler fileHandler = new FileHandler();
            fileHandler.InitDatabase();
            fileHandler.ReadFile(inputFile);
            fileHandler.DoTheFastJob();
            fileHandler.WriteFile(outputFast, fileHandler.GetData());
            fileHandler.DoTheSlowJob();
            fileHandler.WriteFile(outputSlow, fileHandler.GetData());

            //Console.Read();
        }
    }
}
