﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Text.RegularExpressions;

namespace TribalComboReader
{
    public class FileHandler
    {
        private static byte[] buffer;
        private string[] combos;
        private List<string> fast;
        private List<string> slow;
        private StringBuilder finalOutput;
        private Dictionary<string, int> database = new Dictionary<string, int>();

        public void InitDatabase(){
            DatabaseHelper db = new DatabaseHelper();
            db.ReadCSVFromStream();
            database = db.GetData();
        }

        public void ReadFile(string file)
        {
            FileStream stream = new FileStream(file, FileMode.Open, FileAccess.Read);
            if (stream.CanRead)
            {
                buffer = new byte[stream.Length];
                int bytesRead = stream.Read(buffer, 0, buffer.Length);
            }

            stream.Close();

            string pattern = @"\n+\r\n";
            /*
             * Break into separate combos           
             */
            string input = Encoding.ASCII.GetString(buffer, 0, buffer.Length);
            combos = Regex.Split(input, pattern);
        }

        public void WriteFile(string file, string data)
        {
            //string data = finalOutput.ToString();
            FileStream stream = new FileStream(file, FileMode.Create, FileAccess.Write);
            byte[] buffer = new UTF8Encoding(true).GetBytes(data);
            System.Buffer.BlockCopy(buffer, 0, data.ToCharArray(), 0, data.Length);
            if (stream.CanWrite)
            {
                stream.Write(buffer, 0, buffer.Length);
            }

            stream.Flush();
            stream.Close();
        }

        public void DoTheFastJob()
        {
            SeparateFastSlow();
            SeparateDigitsInFast();
        }

        public void DoTheSlowJob()
        {
            SeparateFastSlow();
            SeparateSlow();
        }

        /*
        * Break into fast and slow combos
        */
        private void SeparateFastSlow()
        {
            fast = new List<string>();
            slow = new List<string>();
            foreach (string c in combos)
            {
                if (Regex.Match(c, @"(4|8|16)").Success)
                {
                    fast.Add(c);
                }
                else
                {
                    slow.Add(c);
                }
            }
        }

        /*
        * Break digits in fast
        */
        private void SeparateDigitsInFast()
        {
            string separatedCounts = "";
            var totalCount = 0;
            var a = 0;
            string sepMoves = "";
            StringBuilder output = new StringBuilder();
            finalOutput = new StringBuilder();
            finalOutput.Clear();
            //output.AppendLine("_id,_Combo,_Count,_SeparateCounts,_Level,_Tempo");
            foreach (string move in fast)
            {
                a++;
                foreach (Match m in Regex.Matches(move, @"\s+(4|8|16|24|32)\s*(\r\n|\r|\n)"))
                {
                    if (m.Success)
                    {
                        var d = Regex.Match(m.Value, @"\d+").Value;
                        separatedCounts += ";" + d;
                        totalCount += Int32.Parse(d);
                    }
                }
                separatedCounts = separatedCounts.Remove(0, 1);

                string[] s = Regex.Split(move, @" \d+(?: ?\r\n*|$)");// @"\s+\d+\s*([\r\n]|\r|\n|$)*$");
                s = s.Take(s.Length-1).ToArray();
                int level = checkLevel(s);
                string newS = string.Join(";", s);
                output.Append(a+",");
                output.Append(newS + ",");
                output.Append(totalCount + ",");
                output.Append(separatedCounts + ",");
                output.Append(level + ",");
                output.AppendLine("7");
                finalOutput.Append(output);
                output.Clear();
                separatedCounts = "";
                totalCount = 0;
            }
            finalOutput.Remove(finalOutput.Length - 2,2);
        }

        private void SeparateSlow()
        {
            StringBuilder output = new StringBuilder();
            finalOutput = new StringBuilder();
            finalOutput.Clear();
            //output.AppendLine("_id,_Combo,_Count,_SeparateCounts,_Level,_Tempo");
            int a = 0;
            for (var i = 0; i < slow.Count; i++)
            {
                a++;
                string[] s = Regex.Split(slow[i], @"\r\n*");
                s = s.Take(s.Length - 1).ToArray();
                int level = checkLevel(s);
                string newS = string.Join(";", s);

                output.Append(a + ",");
                output.Append(newS);
                output.Append(",0,0,");
                output.AppendLine(level + ",5");
                finalOutput.Append(output);
                output.Clear();
            }
            finalOutput.Remove(finalOutput.Length - 2, 2);
        }

        public string GetData()
        {
            return finalOutput.ToString();
        }

        private int checkLevel(string[] moves)
        {
            int level = 0;
            foreach (var move in moves)
            {
                if (database.ContainsKey(move))
                {
                    int l = database[move];
                    if (level < l) level = l;
                }                
            }
            return level;
        }
    }
}
