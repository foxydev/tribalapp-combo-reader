﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Kent.Boogaart.KBCsv;

namespace TribalComboReader
{
    class DatabaseHelper
    {
        static string inputFile = @"D:\csharpworkspace\TribalAppComboReader\InputFiles\tribalapp.csv";
        private Dictionary<string, int> database = new Dictionary<string, int>();

        public void ReadCSVFromStream()
        {
            #region ReadCSVFromStream

            using (var stream = new FileStream(inputFile, FileMode.Open))
            using (var reader = new CsvReader(stream, Encoding.UTF8))
            {
                reader.ReadHeaderRecord();

                var i = 1;
                while (reader.HasMoreRecords)
                {
                    var dataRecord = reader.ReadDataRecord();
                    if (!database.ContainsKey(dataRecord["_Name"]))
                    database.Add(dataRecord["_Name"], Int32.Parse(dataRecord["_Level"]));
                }
            }

            #endregion
        }

        public Dictionary<string, int> GetData()
        {
            return database;
        }
    }


}
